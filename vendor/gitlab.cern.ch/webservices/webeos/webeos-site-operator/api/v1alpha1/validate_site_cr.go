package v1alpha1

import (
	"fmt"
	"path"

	"github.com/asaskevich/govalidator"
)

// ValidateSiteDefinitionFromCR will validate a WebEosSite based on the CRs spec
func ValidateSiteDefinitionFromCR(webeossite *WebeosSiteSpec) []error {
	errorList := make([]error, 0)
	errorList = append(errorList, validateOidc(webeossite.Oidc)...)

	if webeossite.Owner == "" {
		errorList = append(errorList, fmt.Errorf("Owner is not defined"))
	}

	if webeossite.HostName == "" {
		errorList = append(errorList, fmt.Errorf("HostName is not defined"))
	}

	legacyBaseURLError := validateLegacyBaseURL(webeossite.LegacyBaseURL, webeossite.HostName)
	if legacyBaseURLError != nil {
		errorList = append(errorList, legacyBaseURLError)
	}
	sitePathError := validateSitePath(webeossite.SitePath)
	if sitePathError != nil {
		errorList = append(errorList, sitePathError)
	}

	errorList = append(errorList, validateConfiguration(webeossite.Configuration)...)
	return errorList
}

func validateOidc(oidc *Oidc) []error {
	if oidc == nil {
		return []error{fmt.Errorf("Oidc is not defined")}
	}

	errors := []error{}
	if oidc.ClientID == "" {
		errors = append(errors, fmt.Errorf("Oidc.ClientID is not defined"))
	}
	if oidc.ClientSecret == "" {
		errors = append(errors, fmt.Errorf("Oidc.ClientSecret is not defined"))
	}
	if oidc.IssuerURL == "" {
		errors = append(errors, fmt.Errorf("Oidc.IssuerURL is not defined"))
	}
	if oidc.ReturnURI == "" {
		errors = append(errors, fmt.Errorf("Oidc.ReturnURI is not defined"))
	}

	return errors
}

func validateLegacyBaseURL(legacyBaseURL *LegacyBaseURL, hostName string) error {
	if legacyBaseURL != nil {
		if legacyBaseURL.Path == "" {
			return fmt.Errorf("The LegacyBaseURL Path is not defined")
		}

		legacyURL := path.Join(hostName, legacyBaseURL.Path)
		if !govalidator.IsURL(legacyURL) {
			return fmt.Errorf("The LegacyBaseURL Path is not valid path: '%v'", legacyBaseURL.Path)
		}
	}

	return nil
}

func validateSitePath(sitePath string) error {
	if sitePath == "" {
		return fmt.Errorf("SitePath is not defined")
	}

	isPath, _ := govalidator.IsFilePath(sitePath)
	if isPath {
		sanitizeSitePath := path.Clean(sitePath)
		if sanitizeSitePath != sitePath {
			return fmt.Errorf("This SitePath is not valid path: '%v'", sitePath)
		}
	} else {
		return fmt.Errorf("This SitePath is not path: '%v'", sitePath)
	}
	return nil
}

func validateConfiguration(configuration []WebeosSiteConfigurationPath) []error {
	errors := []error{}
	for _, v := range configuration {
		isFilepath, _ := govalidator.IsFilePath(v.Path)
		// Check if v.Path is a path
		if isFilepath {
			sanitizePath := path.Clean(v.Path)
			// Check if v.Path contains special characters
			if sanitizePath != v.Path {
				errors = append(errors, fmt.Errorf("This site subfolder path is invalid: %q. Site paths must be in canonical form (no '.' or '..'), start with '/' and must not end with '/'", v.Path))
			}
		} else {
			errors = append(errors, fmt.Errorf("This site subfolder path is invalid because it isn't a path: %q", v.Path))
		}
	}
	return errors
}
