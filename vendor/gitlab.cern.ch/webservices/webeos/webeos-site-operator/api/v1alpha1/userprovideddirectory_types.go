package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// +kubebuilder:validation:Enum=NoIndexing;IndexByCernCrawler;IndexByAllCrawlers
// +kubebuilder:validation:XPreserveUnknownFields

// CrawlingModeConfiguration configuration of the robots.txt
type CrawlingModeConfiguration string

type ServerVersionType string

const (
	ServerVersionCc7 ServerVersionType = "cc7"
	ServerVersionEl9 ServerVersionType = "el9"
)

// UserProvidedDirectorySpec defines the desired state of UserProvidedDirectory
type UserProvidedDirectorySpec struct {
	// +kubebuilder:validation:Pattern:="^/eos(/[^/]+){2,}$"
	// +kubebuilder:validation:Required

	// The existing EOS folder to publish. Follow KB0004096 to configure the EOS folder appropriately.
	EosPath string `json:"eosPath"`

	// Check this is a valid DNS name. OPA rules will further limit what hostnames can be used.
	// NB: a DNS label consisting of only digits is not allowed, but we don't have backtracking in these regex so it's difficult to achieve!
	// +kubebuilder:validation:Pattern:="^([a-zA-Z]|[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9])(\\.([a-zA-Z]|[a-zA-Z0-9][a-zA-Z0-9-]{0,61}[a-zA-Z0-9]))*$"
	// +kubebuilder:validation:Required

	// The desired site URL. Must look like mysite.web.cern.ch and not be used already by another site.
	SiteUrl string `json:"siteUrl"`

	// Global configuration of the webeos site
	GlobalSettings *GlobalSettings `json:"globalSettings,omitempty"`

	// Configuration of subfolders of the webeos site
	FolderConfiguration []WebeosSiteConfigurationPath `json:"folderConfiguration,omitempty"`

	// This setting only takes effect when no robots.txt is present in the web site's root folder.
	// +kubebuilder:default:="IndexByCernCrawler"
	AllowedWebCrawlersIfNoRobotsTxt CrawlingModeConfiguration `json:"allowedWebCrawlersIfNoRobotsTxt,omitempty"`

	// The serverVersion defines from which webeos deployment the site will be served.
	// +kubebuilder:validation:Enum:=cc7;el9
	// +kubebuilder:default:=el9
	ServerVersion ServerVersionType `json:"serverVersion,omitempty"`
}

// GlobalSettings Global configuration of the webeos site
type GlobalSettings struct {

	// Sites not maintained anymore can be archived. A banner indicates the content might not be up to date, and only authenticated users can access it.
	// +kubebuilder:validation:Enum=true;false
	ArchiveSite bool `json:"archiveSite,omitempty"`

	// Sets whether anonymous access is enabled. Per-folder configuration always takes precedence over this setting.
	// When disabled, site visitors must authenticate to access the sites. By default, when anonymous access
	// is disabled, site access is restricted to users member of the EduGain Federation. Site access can be further restricted by defining required roles in
	// the Application Portal.
	// +kubebuilder:validation:Enum=true;false
	AllowAnonymousAccessByDefault bool `json:"allowAnonymousAccessByDefault,omitempty"`

	// Allows to configure the site using `.htaccess` files. If disabled, .htaccess files are ignored and site access will be faster, but configuration options are limited
	// to what is available in Folder Configuration.
	// `true` results in `AllowOverride All`; `false` results in `AllowOverride None`.
	// +kubebuilder:validation:Enum=true;false
	UseDistributedConfigurationFiles bool `json:"useDistributedConfigurationFiles,omitempty"`
}

// UserProvidedDirectoryStatus defines the observed state of UserProvidedDirectory
type UserProvidedDirectoryStatus struct {
	// Conditions represent the latest available observations of an object's state
	Conditions []metav1.Condition `json:"conditions"`
}

const (
	// UpdReady indicates that the the ApplicationRegistration was successful and the WebeosSite CR was created
	UpdReady string = "Ready"
	// UpdFailure indicates that there was a problem with either the ApplicationRegistration or the creation of the WebeosSite CR
	UpdFailure string = "Failure"
)

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status

// UserProvidedDirectory describes a webEOS site served from a pre-existing EOS directory managed by the site's owner.
type UserProvidedDirectory struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   UserProvidedDirectorySpec   `json:"spec,omitempty"`
	Status UserProvidedDirectoryStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// UserProvidedDirectoryList contains a list of UserProvidedDirectory
type UserProvidedDirectoryList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []UserProvidedDirectory `json:"items"`
}

func init() {
	SchemeBuilder.Register(&UserProvidedDirectory{}, &UserProvidedDirectoryList{})
}
